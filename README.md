# Table2Pojo

Sencilla herramienta para generar clases POJO a partir tablas/columnas de base de datos. ###

Soporta todas las bases de datos soportadas por JDBC. Incluye los siguientes controladores JDBC: 
- Oracle 
- MySQL 
- PostgreSQL 
- DB2

Los controladores JDBC se pueden agregar/actualizar en el archivo `build.gradle`.

## Build

Use gradle para construir y ensamblar archivos jar con dependencias. 
```bash
gradle fatJar
```

## Ejecutar

Configure las propiedades de conexión de la base de datos en el archivo `db.properties` en el directorio actual:

```
driver=oracle.jdbc.OracleDriver
url=jdbc:oracle:thin:@localhost:1521:orcl
username=dbusername
password=dbpassword
```

Ejecutar el JAR con argumentos. 
```bash
java -jar build/libs/table2pojo-all.jar <options>
```

## Opciones

Opcion | Descripción
-------|------------
h | ayuda 
a | generar POJOs para todas las tablas en la base de datos 
t | lista de tablas de bases de datos delimitadas por; (punto y coma). anula la opción `a` 
p | (opcional) nombre del paquete java de los POJOs. Si no se especifica, se utilizará el paquete predeterminado / en blanco.
d | (opcional) directorio de destino donde se generan POJOs (archivos .Java). Si no se especifica, se utilizará el directorio actual.
